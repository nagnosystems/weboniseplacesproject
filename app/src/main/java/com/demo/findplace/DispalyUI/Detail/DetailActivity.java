package com.demo.findplace.DispalyUI.Detail;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.TextView;

import com.demo.basearch.BaseActivity;
import com.demo.findplace.DispalyUI.SearchPlace.PlacesAdapter;
import com.demo.findplace.POJO.GeometryResData;
import com.demo.findplace.POJO.LocResData;
import com.demo.findplace.POJO.PicRespData;
import com.demo.findplace.POJO.PlaceItem;
import com.demo.findplace.R;
import com.demo.findplace.Utilities.IntentConstants;
import com.demo.findplace.cache.PlacesCache;
import com.demo.findplace.retofit.NetworkAPI;
import com.demo.findplace.retofit.RequestGenerator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends  BaseActivity<DetailActivityContract.View, DetailActivityContract.Presenter>
        implements DetailActivityContract.View , PicsAdapter.PicsItemListener , OnMapReadyCallback {

    private static final String TAG = DetailActivity.class.getSimpleName();
    private GoogleMap mMap;
    private PlaceItem placeItem;
    private RecyclerView rvPics;
    private List<PicRespData> list;
    private PicsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if(getIntent().getExtras()!=null)
        {
            String data = getIntent().getExtras().getString(IntentConstants.PLACE_DATA);
            placeItem = new Gson().fromJson(data, new TypeToken<PlaceItem>() {
            }.getType());
            PlacesCache placesCache = new PlacesCache(getApplicationContext());
            placesCache.savePlaceItem(placeItem);
        }

        initViews();

        presenter.loadPlaceDetails(placeItem.place_id);

    }

    @Override
    protected DetailActivityContract.Presenter initPresenter() {
        return new DetailActivityPresenter(RequestGenerator.createService(NetworkAPI.class));
    }

    private void initViews()
    {
        rvPics = findViewById(R.id.rvPics);
        list = new ArrayList<>();
        adapter = new PicsAdapter(list,this, this);
        rvPics.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvPics.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (placeItem != null) {
            GeometryResData geometryResData = placeItem.geometry;
            if (geometryResData != null) {
                LocResData location = geometryResData.location;
                if (location != null) {
                    if (location.lat != 0 && location.lng != 0) {
                        LatLng latLng = new LatLng(location.lat, location.lng);
                        mMap.addMarker(new MarkerOptions().position(latLng).title(placeItem.name));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f));
                    }
                }
            }
        }

    }

    @Override
    public void onPlaceFetch(PlaceItem place) {

        if(null!=place && null!=place.photos && place.photos.size()>0)
        {
            list.clear();
            list.addAll(place.photos);
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onPlaceFetchFail(int resId) {

    }

    @Override
    public void showMessage(int resID) {

    }

    @Override
    public void onPicsSetected(int position) {

    }
}
