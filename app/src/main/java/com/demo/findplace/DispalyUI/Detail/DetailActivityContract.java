package com.demo.findplace.DispalyUI.Detail;

import com.demo.basearch.BaseContract;
import com.demo.findplace.POJO.PlaceItem;

import java.util.List;

public interface DetailActivityContract {

    interface View extends BaseContract.View {

        void onPlaceFetch(PlaceItem place);
        void onPlaceFetchFail(int resId);
        void showMessage(int resID);

    }

    interface Presenter extends BaseContract.Presenter<DetailActivityContract.View> {

        void loadPlaceDetails(String placeID);
    }
}
