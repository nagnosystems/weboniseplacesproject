package com.demo.findplace.DispalyUI.Detail;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.Handler;
import android.util.Log;

import com.demo.basearch.BasePresenter;
import com.demo.findplace.DispalyUI.SearchPlace.SearchActivityContract;
import com.demo.findplace.POJO.PlaceDetailResponse;
import com.demo.findplace.POJO.SearchResponse;
import com.demo.findplace.R;
import com.demo.findplace.retofit.NetworkAPI;
import com.demo.findplace.retofit.RequestGenerator;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class DetailActivityPresenter extends BasePresenter<DetailActivityContract.View> implements DetailActivityContract.Presenter {

    private static final String TAG = DetailActivityPresenter.class.getSimpleName();
    private Handler handler = new Handler();
    private NetworkAPI networkAPI;

    public DetailActivityPresenter(NetworkAPI networkAPI) {
        this.networkAPI = networkAPI;
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_CREATE)
    protected void onCreate() {

    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_DESTROY)
    protected void onDestroy() {


    }

    @Override
    public void onPresenterDestroy() {
        super.onPresenterDestroy();
        Log.d(TAG, "Presenter destroyed");
    }

    @Override
    public void loadPlaceDetails(String placeID) {
        Observable<PlaceDetailResponse> call = networkAPI.getPlaceDetails(placeID,"AIzaSyB-0sIcmmnyVkETrsYpZaO8r5i6KJLszaw");
        call.subscribeOn(Schedulers.io())
                .debounce(200, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<PlaceDetailResponse>() {
                    @Override
                    public void onNext(PlaceDetailResponse response) {

                        if (isViewAttached()) {
                            if(null!=response && null!= response.result)
                            {
                                getView().onPlaceFetch(response.result);
                            }
                            else
                            {
                                getView().showMessage(R.string.no_data_found);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (isViewAttached()) {
                            getView().showMessage(R.string.went_wrong);
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
