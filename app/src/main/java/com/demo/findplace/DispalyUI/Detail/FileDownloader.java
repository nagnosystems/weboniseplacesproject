package com.demo.findplace.DispalyUI.Detail;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;

import io.reactivex.functions.Consumer;

public class FileDownloader {

    private Context context;
    private DownloadManager downloadManager;
    private RxPermissions rxPermissions;

    public FileDownloader(Context context, RxPermissions rxPermissions) {
        this.context = context.getApplicationContext();
        this.rxPermissions = rxPermissions;
        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    public void startDownloading(final String downloadUrlPath) {
        final String downloadName = String.valueOf(System.currentTimeMillis());
        rxPermissions.requestEach(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(new Consumer<Permission>() {
                    @Override
                    public void accept(Permission permission) throws Exception {
                        if (permission.granted) {
                            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + downloadName;
                            File outputFile = new File(path);
                            if (outputFile.exists()) {
                                outputFile.delete();
                            }

                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(downloadUrlPath));
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, downloadName);
                            downloadManager.enqueue(request);

                        } else if (permission.shouldShowRequestPermissionRationale) {
                            Toast.makeText(context, "Write Storage permission denied. Please enable from settings", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(context, "Write Storage permission denied.", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(context, "Write Storage permission denied.", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
