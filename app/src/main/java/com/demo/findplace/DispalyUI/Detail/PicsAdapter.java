package com.demo.findplace.DispalyUI.Detail;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.findplace.POJO.PicRespData;
import com.demo.findplace.POJO.PlaceItem;
import com.demo.findplace.R;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;


public class PicsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PicRespData> listItems;
    private PicsItemListener listener;
    private Context context;
    private FileDownloader fileDownloader;

    public PicsAdapter(List<PicRespData> listItems, PicsItemListener listener,Context context)
    {
        this.listItems = listItems;
        this.listener = listener;
        this.context = context;
        RxPermissions rxPermissions = new RxPermissions((Activity) context);
        fileDownloader = new FileDownloader(context, rxPermissions);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater layoutInflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_pics, null);
        holder = new PicsViewHolder(view,listener);
        return holder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PicsViewHolder placeViewHolder = (PicsViewHolder)holder;

        final String url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=150&photoreference="
                + listItems.get(position).photo_reference
                + "&key=AIzaSyB-0sIcmmnyVkETrsYpZaO8r5i6KJLszaw";
        Picasso.with(context)
                .load(url)
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(((PicsViewHolder) holder).ivPics);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(context, "Downloading image. Please wait..", Toast.LENGTH_SHORT).show();
                fileDownloader.startDownloading(url);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        if(listItems != null)
            return listItems.size();
        else
            return 0;
    }

    public static class PicsViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView ivPics;
        public PicsViewHolder(View v, final PicsItemListener listener)
        {
            super(v);
            ivPics = (ImageView) v.findViewById(R.id.ivPic);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null)
                    {
                        listener.onPicsSetected(getAdapterPosition());
                    }
                }
            });
        }
    }

    interface PicsItemListener
    {
        void onPicsSetected(int position);
    }

}