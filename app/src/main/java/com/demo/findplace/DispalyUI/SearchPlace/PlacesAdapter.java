package com.demo.findplace.DispalyUI.SearchPlace;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demo.findplace.POJO.PlaceItem;
import com.demo.findplace.R;

import java.util.List;


public class PlacesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PlaceItem> listItems;
    private PlaceItemListener listener;

    public PlacesAdapter(List<PlaceItem> listItems, PlaceItemListener listener)
    {
        this.listItems = listItems;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater layoutInflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_place, null);
        holder = new PlaceViewHolder(view,listener);
        return holder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PlaceViewHolder placeViewHolder = (PlaceViewHolder)holder;
        placeViewHolder.tvPlace.setText(listItems.get(position).name);
    }

    @Override
    public int getItemCount() {
        if(listItems != null)
            return listItems.size();
        else
            return 0;
    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder
    {
        public TextView tvPlace;
        public PlaceViewHolder(View v, final PlaceItemListener listener)
        {
            super(v);
            tvPlace = (TextView)v.findViewById(R.id.tvPlace);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null)
                    {
                        listener.onPlaceSetected(getAdapterPosition());
                    }
                }
            });
        }
    }

    interface PlaceItemListener
    {
        void onPlaceSetected(int position);
    }

}