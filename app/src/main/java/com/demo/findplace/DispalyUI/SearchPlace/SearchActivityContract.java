package com.demo.findplace.DispalyUI.SearchPlace;

import com.demo.basearch.BaseContract;
import com.demo.findplace.POJO.PlaceItem;

import java.util.List;

public interface SearchActivityContract {

    interface View extends BaseContract.View {

        void showDetailScreen();
        void onPlacesFetch(List<PlaceItem> list);
        void onPlacesFetchFail(int resId);
        void showMessage(int resID);
        void showMessage(String resID);

    }

    interface Presenter extends BaseContract.Presenter<SearchActivityContract.View> {

        void loadPlaces(String searchTxt);
    }
}
