package com.demo.findplace.DispalyUI.SearchPlace;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.util.Log;

import com.demo.basearch.BasePresenter;
import com.demo.findplace.POJO.PlaceItem;
import com.demo.findplace.POJO.SearchResponse;
import com.demo.findplace.R;
import com.demo.findplace.cache.PlacesCache;
import com.demo.findplace.retofit.NetworkAPI;
import com.demo.findplace.retofit.RequestGenerator;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class SearchActivityPresenter extends BasePresenter<SearchActivityContract.View> implements SearchActivityContract.Presenter {

    private static final String TAG = SearchActivityPresenter.class.getSimpleName();
    private Disposable disposable;
    private PlacesCache placesCache;
    private NetworkAPI networkAPI;

    public SearchActivityPresenter(PlacesCache placesCache, NetworkAPI networkAPI) {
        this.placesCache = placesCache;
        this.networkAPI = networkAPI;
    }

    @Override
    public void loadPlaces(String searchTxt) {
        if (disposable != null) {
            disposable.dispose();
        }

        if (searchTxt == null || searchTxt.isEmpty()) {
            List<PlaceItem> savedPlaceItems = placesCache.getSavedPlaceItems();
            getView().onPlacesFetch(savedPlaceItems);
            return;
        }

        if (searchTxt.trim().length() > 3) {
            Observable<SearchResponse> call = networkAPI.getPlaces(searchTxt, "AIzaSyB-0sIcmmnyVkETrsYpZaO8r5i6KJLszaw");
            disposable = call.subscribeOn(Schedulers.io())
                    .debounce(200, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<SearchResponse>() {
                        @Override
                        public void accept(SearchResponse response) throws Exception {

                            if (isViewAttached()) {
                                if (null != response) {
                                    if (response.status.equals("OK")) {
                                        if (null != response.results && response.results.size() > 0) {
                                            getView().onPlacesFetch(response.results);
                                        } else {
                                            getView().showMessage(response.error_message);
                                        }
                                    } else {
                                        getView().showMessage(response.error_message);
                                    }
                                }

                            }

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {

                            if (isViewAttached()) {
                                getView().showMessage(R.string.went_wrong);
                            }

                        }
                    });
        }
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_CREATE)
    protected void onCreate() {

    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_DESTROY)
    protected void onDestroy() {
        if (disposable != null) {
            disposable.dispose();
        }
    }

    @Override
    public void onPresenterDestroy() {
        super.onPresenterDestroy();
        Log.d(TAG, "Presenter destroyed");
    }
}
