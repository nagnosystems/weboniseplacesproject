package com.demo.findplace.DispalyUI.SearchPlace;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.basearch.BaseActivity;
import com.demo.findplace.DispalyUI.Detail.DetailActivity;
import com.demo.findplace.POJO.PlaceItem;
import com.demo.findplace.R;
import com.demo.findplace.Utilities.IntentConstants;
import com.demo.findplace.cache.PlacesCache;
import com.demo.findplace.retofit.NetworkAPI;
import com.demo.findplace.retofit.RequestGenerator;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.List;

public class SearchPlaceActivity extends BaseActivity<SearchActivityContract.View, SearchActivityContract.Presenter>
        implements SearchActivityContract.View , PlacesAdapter.PlaceItemListener {

    private RecyclerView rvPlace;
    private EditText editSearch;
    private TextView txtInfo;
    private List<PlaceItem> list;
    private PlacesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_place);
        initViews();
        presenter.loadPlaces(editSearch.getText().toString().trim());
    }

    @Override
    protected SearchActivityContract.Presenter initPresenter() {
        PlacesCache placesCache = new PlacesCache(getApplicationContext());
        NetworkAPI networkAPI = RequestGenerator.createService(NetworkAPI.class);
        return new SearchActivityPresenter(placesCache, networkAPI);
    }

    private void initViews()
    {
        editSearch = findViewById(R.id.editSearch);
        rvPlace = findViewById(R.id.rvPlace);
        txtInfo = findViewById(R.id.txtInfo);
        list = new ArrayList<>();
        adapter = new PlacesAdapter(list,this);
        rvPlace.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvPlace.setLayoutManager(linearLayoutManager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(rvPlace.getContext(),linearLayoutManager.getOrientation());
        itemDecoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.item_decor));
        rvPlace.addItemDecoration(itemDecoration);

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.loadPlaces(charSequence.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private void openDetailScreen(int position)
    {
        if(position>-1)
        {
            Intent intent = new Intent(this,DetailActivity.class);
            String data = new Gson().toJson(list.get(position));
            Bundle bd = new Bundle();
            bd.putString(IntentConstants.PLACE_DATA,data);
            intent.putExtras(bd);
            startActivity(intent);
        }
    }

    @Override
    public void showDetailScreen() {

    }

    @Override
    public void onPlacesFetch(List<PlaceItem> list) {
        if(list!=null && list.size()>0)
        {
            this.list.clear();
            this.list.addAll(list);
            adapter.notifyDataSetChanged();
            txtInfo.setVisibility(View.GONE);
            rvPlace.setVisibility(View.VISIBLE);
        }
        else
        {
            txtInfo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPlacesFetchFail(int resId) {
        txtInfo.setVisibility(View.VISIBLE);
        txtInfo.setText(resId);
    }

    @Override
    public void showMessage(int resID) {
        txtInfo.setVisibility(View.VISIBLE);
        txtInfo.setText(resID);
    }

    @Override
    public void showMessage(String resID) {
        txtInfo.setVisibility(View.VISIBLE);
        txtInfo.setText(resID);
    }

    @Override
    public void onPlaceSetected(final int position) {

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override public void onPermissionGranted(PermissionGrantedResponse response) {
                        openDetailScreen(position);
                    }
                    @Override public void onPermissionDenied(PermissionDeniedResponse response) {
                        Toast.makeText(SearchPlaceActivity.this,"Please grant premission to proceed", Toast.LENGTH_LONG).show();

                    }
                    @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
                }).check();


    }
}
