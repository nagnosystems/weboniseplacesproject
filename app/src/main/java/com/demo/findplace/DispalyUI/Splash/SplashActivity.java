package com.demo.findplace.DispalyUI.Splash;

import android.content.Intent;
import android.os.Bundle;

import com.demo.basearch.BaseActivity;
import com.demo.findplace.R;
import com.demo.findplace.DispalyUI.SearchPlace.SearchPlaceActivity;

public class SplashActivity extends BaseActivity<SplashActivityContract.View, SplashActivityContract.Presenter>
        implements SplashActivityContract.View {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        presenter.doLaunchMainScreen(3000L);
    }

    @Override
    protected SplashActivityContract.Presenter initPresenter() {
        return new SplashActivityPresenter();
    }

    @Override
    public void showMainScreen() {
        Intent intent =  new Intent(this, SearchPlaceActivity.class);
        startActivity(intent);
        finish();
    }
}
