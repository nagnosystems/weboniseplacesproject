package com.demo.findplace.DispalyUI.Splash;

import com.demo.basearch.BaseContract;

public interface SplashActivityContract {

    interface View extends BaseContract.View {

        void showMainScreen();
    }

    interface Presenter extends BaseContract.Presenter<SplashActivityContract.View> {

        void doLaunchMainScreen(long delay);
    }
}
