package com.demo.findplace.DispalyUI.Splash;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.Handler;
import android.util.Log;

import com.demo.basearch.BasePresenter;

public class SplashActivityPresenter extends BasePresenter<SplashActivityContract.View> implements SplashActivityContract.Presenter {

    private static final String TAG = SplashActivityPresenter.class.getSimpleName();
    private Handler handler = new Handler();

    @Override
    public void doLaunchMainScreen(long delay) {

        if(handler!=null)
        {
            handler.removeCallbacksAndMessages(null);
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isViewAttached()) {
                    getView().showMainScreen();
                }
            }
        },delay);
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_CREATE)
    protected void onCreate() {

    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_DESTROY)
    protected void onDestroy() {

    }

    @Override
    public void onPresenterDestroy() {
        super.onPresenterDestroy();
        Log.d(TAG, "Presenter destroyed");
    }
}
