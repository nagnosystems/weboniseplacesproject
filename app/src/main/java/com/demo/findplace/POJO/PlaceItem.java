package com.demo.findplace.POJO;

import java.util.List;



public class PlaceItem {

    public String icon;
    public String name;
    public String place_id;
    public float rating;
    public String vicinity;
    public GeometryResData geometry;
    public List<PicRespData> photos;
    public String formatted_address;
    public String formatted_phone_number;

}
