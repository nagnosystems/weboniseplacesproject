package com.demo.findplace.POJO;

import java.util.List;

public class SearchResponse {

    public List<PlaceItem> results;
    public String status;
    public String error_message;

}
