package com.demo.findplace.Utilities;



public class IntentConstants {

    public static final String INTENT_PERMISSION_RESULT = "INTENT_PERMISSION_RESULT";
    public static final String INTENT_PERMISSION_LIST = "INTENT_PERMISSION_LIST";

    public static final String PLACE_DATA = "PLACE_DATA";


}
