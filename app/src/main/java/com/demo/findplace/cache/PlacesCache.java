package com.demo.findplace.cache;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.demo.findplace.POJO.PlaceItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlacesCache {

    private static final String KEY_PLACE_ITEMS = "place_items";
    private SharedPreferences sharedPreferences;
    private Gson gson = new Gson();

    public PlacesCache(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public List<PlaceItem> getSavedPlaceItems() {
        String placeItemString = sharedPreferences.getString(KEY_PLACE_ITEMS, "");
        if (!placeItemString.isEmpty()) {
            Map<String, PlaceItem> retMap = new Gson().fromJson(
                    placeItemString,
                    new TypeToken<HashMap<String, PlaceItem>>() {
                    }.getType()
            );
            return new ArrayList<>(retMap.values());
        } else {
            return new ArrayList<>();
        }
    }

    public void savePlaceItem(PlaceItem placeItem) {
        String placeItemString = sharedPreferences.getString(KEY_PLACE_ITEMS, "");
        Map<String, PlaceItem> savedPlaceItemMap;
        if (!placeItemString.isEmpty()) {
            Map<String, PlaceItem> retMap = new Gson().fromJson(
                    placeItemString,
                    new TypeToken<HashMap<String, PlaceItem>>() {
                    }.getType()
            );
            savedPlaceItemMap = retMap;
        } else {
            savedPlaceItemMap = new HashMap<>();
        }
        savedPlaceItemMap.put(placeItem.place_id, placeItem);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_PLACE_ITEMS, gson.toJson(savedPlaceItemMap));
        editor.apply();
    }

}
