package com.demo.findplace.retofit;

import com.demo.findplace.POJO.PlaceDetailResponse;
import com.demo.findplace.POJO.SearchResponse;


import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NetworkAPI {

    @GET("maps/api/place/textsearch/json")
    Observable<SearchResponse> getPlaces(@Query("query") String query, @Query("key") String key);

    @GET("maps/api/place/details/json")
    Observable<PlaceDetailResponse> getPlaceDetails(@Query("placeid") String placeid, @Query("key") String key);

}
