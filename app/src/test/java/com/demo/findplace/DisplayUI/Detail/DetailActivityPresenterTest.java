package com.demo.findplace.DisplayUI.Detail;

import com.demo.findplace.DispalyUI.Detail.DetailActivityContract;
import com.demo.findplace.DispalyUI.Detail.DetailActivityPresenter;
import com.demo.findplace.POJO.PlaceDetailResponse;
import com.demo.findplace.POJO.PlaceItem;
import com.demo.findplace.retofit.NetworkAPI;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collections;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class DetailActivityPresenterTest {

    @Mock
    private NetworkAPI networkAPI;

    @Mock
    private DetailActivityContract.View view;

    private DetailActivityPresenter detailActivityPresenter;

    @BeforeClass
    public static void setUpClass() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @Before
    public void setUp() {
        detailActivityPresenter = new DetailActivityPresenter(networkAPI);
        detailActivityPresenter.attachView(view);
    }

    @Test
    public void test_PlaceDetails_With_Valid_Response() {
        PlaceItem placeItem = mock(PlaceItem.class);
        PlaceDetailResponse placeDetailResponse = new PlaceDetailResponse();
        placeDetailResponse.result = placeItem;
        when(networkAPI.getPlaceDetails(Matchers.anyString(), Matchers.anyString())).thenReturn(Observable.just(placeDetailResponse));

        detailActivityPresenter.loadPlaceDetails("ff");

        verify(view, times(1)).onPlaceFetch(placeItem);
    }
}
