package com.demo.findplace.DisplayUI.SearchPlace;

import com.demo.findplace.DispalyUI.SearchPlace.SearchActivityContract;
import com.demo.findplace.DispalyUI.SearchPlace.SearchActivityPresenter;
import com.demo.findplace.POJO.PlaceItem;
import com.demo.findplace.POJO.SearchResponse;
import com.demo.findplace.cache.PlacesCache;
import com.demo.findplace.retofit.NetworkAPI;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
public class SearchActivityPresenterTest {

    @Mock
    private NetworkAPI networkAPI;

    @Mock
    private PlacesCache placesCache;

    @Mock
    private SearchActivityContract.View view;

    private SearchActivityPresenter searchActivityPresenter;

    @BeforeClass
    public static void setUpBeforeClass() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @Before
    public void setUp() {
        searchActivityPresenter = new SearchActivityPresenter(placesCache, networkAPI);
        searchActivityPresenter.attachView(view);
    }

    @Test
    public void test_LoadData_With_Empty_Keyword_And_Cache_Also_Empty() {
        when(placesCache.getSavedPlaceItems()).thenReturn(Collections.<PlaceItem>emptyList());
        searchActivityPresenter.loadPlaces("");
        verify(view, times(1)).onPlacesFetch(Collections.<PlaceItem>emptyList());
    }

    @Test
    public void test_LoadData_With_Empty_Keyword_And_Cache_Not_Empty() {
        List<PlaceItem> placeItemList = new ArrayList<>();
        placeItemList.add(mock(PlaceItem.class));
        when(placesCache.getSavedPlaceItems()).thenReturn(placeItemList);
        searchActivityPresenter.loadPlaces("");
        verify(view, times(1)).onPlacesFetch(placeItemList);
    }

    @Test
    public void test_LoadData_With_Keyword_Of_Length_Less_Than_Three() {
        List<PlaceItem> placeItemList = new ArrayList<>();
        placeItemList.add(mock(PlaceItem.class));
        searchActivityPresenter.loadPlaces("");
        verify(view, times(0)).onPlacesFetch(placeItemList);
    }

    @Test
    public void test_LoadData_With_Keyword_Of_Length_Greater_Than_Three() {
        SearchResponse searchResponse = new SearchResponse();
        searchResponse.status = "OK";
        List<PlaceItem> placeItemList = new ArrayList<>();
        placeItemList.add(mock(PlaceItem.class));
        searchResponse.results = placeItemList;
        when(networkAPI.getPlaces(Matchers.anyString(), Matchers.anyString())).thenReturn(Observable.just(searchResponse));


        searchActivityPresenter.loadPlaces("");
        verify(view, times(0)).onPlacesFetch(placeItemList);
    }

    @Test
    public void test_LoadData_When_Response_Is_Error() {
        SearchResponse searchResponse = new SearchResponse();
        searchResponse.status = "Error";
        searchResponse.error_message = "Invalid request";
        when(networkAPI.getPlaces(Matchers.anyString(), Matchers.anyString())).thenReturn(Observable.just(searchResponse));

        searchActivityPresenter.loadPlaces("");
        verify(view, times(0)).showMessage("Invalid request");
    }
}
